-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

vim.g.autoformat = false

if vim.g.neovide then
  vim.o.guifont = "JetBrainsMonoNL Nerd Font:h14"
  vim.g.neovide_cursor_animation_length = 0.05
end

-- if alpha + beta < |DELTA| , then do this
-- x = alpha + e^beta * x
